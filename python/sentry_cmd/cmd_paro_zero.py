#!/usr/bin/env python

import rospy
from ds_core_msgs.srv import VoidCmd

class CmdZeroParo(object):
    """Implements a command to control the controller type"""

    nodename = '/sentry/sensors/paro'

    def __init__(self):
        self._contype = None

    def parse_args(self, args):
        """Parse all arguments.  Either throw an execption, or be ready-to-run"""
        pass
        # This command has no arguments

    def execute(self):
        """Actually execute the commands.  Args is a list of string arguments from argparse"""


        print 'Setting the paro at %s to 0m' % CmdZeroParo.nodename
        zero_srv = rospy.ServiceProxy(CmdZeroParo.nodename + '/zero_depth', VoidCmd)
        if zero_srv.call():
            print 'Paro depth reset!'

    @staticmethod
    def help():
        print """
        
        Set the paro's current depth to 0m by resetting the driver's offset while leaving the paro's own
        TARE untouched.
        
        sentry_cmd zero_paro
        
        """

    @staticmethod
    def args_help():
        return ''

# Expose this class to the wider world
CmdName = 'ZERO_PARO'
CmdClass = CmdZeroParo

