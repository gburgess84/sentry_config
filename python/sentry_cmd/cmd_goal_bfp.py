#!/usr/bin/env python

import ds_param

class CmdGoalBf(object):
    """Implements a command to control a goal param node"""

    bf_ns = '/sentry/goals/bottom_follower'
    params = ['altitude',
              'envelope',
              'speed',
              'speed_gain',
              'depth_rate',
              'depth_acceleration',
              'speed_minimum',
              'alarm_timeout',
              'depth_floor']

    def __init__(self):

        self._param = None
        self._value = None

    def parse_args(self, args):
        """Parse all arguments.  Either throw an execption, or be ready-to-run"""
        if not args:
            raise ValueError('No parameters supplied')


        if len(args) < 2:
            raise ValueError('Expected $param_name $value pair')

        self._param = args[0]
        if self._param not in CmdGoalBf.params:
            raise 'Unknown parameter %s' % self._param

        self._value = float(args[1])

    def execute(self):
        """Actually execute the commands.  Args is a list of string arguments from argparse"""

        full_param = CmdGoalBf.bf_ns + '/' + self._param
        print 'Setting parameter \"%s\" to %f' % (full_param, self._value)

        conn = ds_param.ParamConnection()

        p = conn.connect(full_param, ds_param.DoubleParam, False)
        p.set(self._value)

    @staticmethod
    def help():
        print """
        
        Set bottom follower parameter
        
        sentry_cmd goal_param [param_name] [param_value]
        
        The parameter names are:
        """

        for p in CmdGoalBf.params:
            print '\t    %s' % p
        print '\n'
        

    @staticmethod
    def args_help():
        return '[param_name] [param_value]'

# Expose this class to the wider world
CmdName = 'GOAL_BF'
CmdClass = CmdGoalBf

