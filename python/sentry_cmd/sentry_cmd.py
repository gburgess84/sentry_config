import sys
import argparse

import rospy

# Register commands
cmd_map = {}

import cmd_con
cmd_map[cmd_con.CmdName.lower()] = cmd_con.CmdClass

import cmd_alloc
cmd_map[cmd_alloc.CmdName.lower()] = cmd_alloc.CmdClass

import cmd_leg
cmd_map[cmd_leg.CmdName.lower()] = cmd_leg.CmdClass

import cmd_paro_zero
cmd_map[cmd_paro_zero.CmdName.lower()] = cmd_paro_zero.CmdClass

import cmd_goal_bfp
cmd_map[cmd_goal_bfp.CmdName.lower()] = cmd_goal_bfp.CmdClass

import cmd_goal_param
cmd_map[cmd_goal_param.CmdName.lower()] = cmd_goal_param.CmdClass

def get_command(cmdstr):
    """Instantiate a command object.  These define help() and execute(args)"""
    return cmd_map[cmdstr.lower()]()

def print_command_help():
    print """
    The following commands are registered for use: 
    
    """
    print '\t%20s %s\n' % ('HELP', '[other command]')
    for cmd,cmd_class in cmd_map.iteritems():
        print '\t%20s %s\n' % (cmd.upper(),cmd_class.args_help())

    print '\n Command names are parsed case-insensitively'
    print ''

def main():
    parser = argparse.ArgumentParser(description='Send a commmand to Sentry')
    parser.add_argument('command', help='Command to use.  Probably \"HELP\" or \"LEG\" or \"CON\" or.. something?')
    parser.add_argument('args', nargs='*', help='Additional arguments for that command')

    args = parser.parse_args()

    if args.command.lower() == 'help':
        if not args.args:
            print_command_help()
            sys.exit(1)

        cmdstr = args.args[0]
        print '\nViewing help for command \"%s\"...' % cmdstr

        cmd = get_command(cmdstr)
        cmd.help()
        sys.exit(1)

    else:

        cmd = get_command(args.command)

        # Parse arguments ahead of time so they can fail before we start ROS
        cmd.parse_args(args.args)

        rospy.init_node('sentry_cmd', anonymous=True)
        cmd.execute()
        ONCE_DELAY = 3.0 # See line 1670 of rostopic
        print 'Sleeping for %.3f seconds like rostopic pub does so that ' \
              'other processes have time to pick up our transmission' % ONCE_DELAY

        rospy.sleep(ONCE_DELAY)

