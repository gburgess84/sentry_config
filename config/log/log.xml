<launch>
    <arg name="logdir"/>
    <arg name="log_sim"/>

    <arg name="vehicle_ns"/>

    <arg name="sensors_ns"/>
    <arg name="devices_ns"/>
    <arg name="nav_ns"/>
    <arg name="model_ns"/>
    <arg name="hotel_ns"/>
    <arg name="mission_ns"/>
    <arg name="dynamics_ns"/>
    <arg name="controllers_ns"/>
    <arg name="goals_ns"/>
    <arg name="actuator_ns"/>
    <arg name="acomms_ns"/>
    <arg name="sim_ns"/>
    <arg name="sonars_ns"/>
    <arg name="pkz_ns"/>
    
    <!-- Split the files once per hour, but NOT at the top of the hour -->
    <arg name="split_opts" default="--split --duration=1h"/>

    <!-- Internal arguments -->
    <arg name="global_regex" value="(?:/[^/]*$)|(?:$(arg vehicle_ns)/[^/]*$)|(?:/tf)|(?:/tf_static)|(?:/updating_param/.*)|(?:/clock)|(?:$(arg vehicle_ns)/log/.*)"/>
    <arg name="exclude_namespaces" value="(?:^$(arg sensors_ns).*)|(?:^$(arg devices_ns).*)|(?:^$(arg nav_ns).*)|(?:^$(arg model_ns).*)|(?:^$(arg hotel_ns).*)|(?:^$(arg mission_ns).*)|(?:^$(arg dynamics_ns).*)|(?:^$(arg controllers_ns).*)|(?:^$(arg goals_ns).*)|(?:^$(arg actuator_ns).*)|(?:^$(arg acomms_ns).*)|(?:^$(arg sonars_ns).*)|(?:^$(arg pkz_ns).*)"/>
    <!-- A note on regexes:
         We have to deal with escaping here.  The raw regexes we're using are:
         To match only raw topics:  "/raw$"
         To match only topics in a given namespace: "^/namespace/.*"
         To match raw topics in a given namespace: "^/namespace/?.*/raw$
         -->
    <group ns="log">

        <!-- Start two rosbags for each namespace- one including raw and one excluding it -->
        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="sensors"/>
            <arg name="namespace" value="$(arg sensors_ns)"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="devices"/>
            <arg name="namespace" value="$(arg devices_ns)"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="nav"/>
            <arg name="namespace" value="$(arg nav_ns)"/>
            <arg name="lograw" value="false"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="model"/>
            <arg name="namespace" value="$(arg model_ns)"/>
            <arg name="lograw" value="false"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="hotel"/>
            <arg name="namespace" value="$(arg hotel_ns)"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="mission"/>
            <arg name="namespace" value="$(arg mission_ns)"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="dynamics"/>
            <arg name="namespace" value="$(arg dynamics_ns)"/>
            <arg name="lograw" value="false"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="controllers"/>
            <arg name="namespace" value="$(arg controllers_ns)"/>
            <arg name="lograw" value="false"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="goals"/>
            <arg name="namespace" value="$(arg goals_ns)"/>
            <arg name="lograw" value="false"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="actuators"/>
            <arg name="namespace" value="$(arg actuator_ns)"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="acomms"/>
            <arg name="namespace" value="$(arg acomms_ns)"/>
            <arg name="lograw" value="true"/>
        </include>

        <include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="sonars"/>
            <arg name="namespace" value="$(arg sonars_ns)"/>
            <arg name="lograw" value="true"/>
    	</include>
	
	<include file="$(find sentry_config)/config/log/namespace_logger.xml">
            <arg name="logdir" value="$(arg logdir)"/>
            <arg name="split_opts" value="$(arg split_opts)"/>

            <arg name="basename" value="pkz"/>
            <arg name="namespace" value="$(arg pkz_ns)"/>
            <arg name="lograw" value="true"/>
        </include>


        <!-- Log simulator stuff -->
        <node pkg="rosbag" type="record" name="sim"
              args="record -o $(arg logdir)/sim $(arg split_opts) -e '(?:^$(arg sim_ns)/.*)|(?:/gazebo/.*)'"
              if="$(arg log_sim)"/>

        <!-- Start a rosbag for console traffic -->
        <node pkg="rosbag" type="record" name="rosconsole"
              args="record -o $(arg logdir)/rosconsole $(arg split_opts) -e '^/rosout_agg'"/>

        <!-- Start a rosbag for updating params, global stuff -->
        <node pkg="rosbag" type="record" name="log_globals"
              args="record -o $(arg logdir)/globals $(arg split_opts) -e '$(arg global_regex)'"/>

        <!-- Start a rosbag to record everything else -->
        <node pkg="rosbag" type="record" name="other"
              args="record -o $(arg logdir)/other $(arg split_opts) -a -x '$(arg global_regex)|$(arg exclude_namespaces)|(?:^/rosout$)|(?:^/rosout_agg$)|(?:^$(arg sim_ns)/.*)|(?:/gazebo/.*)'"/>
    </group>
</launch>
