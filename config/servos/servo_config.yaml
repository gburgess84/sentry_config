# Parameters!
# The Copley motor controller has hundreds of parameters; this file
# is a summary of them, in addition to specifying the ones we actually
# care about.
#
# Values provided are from the most recent param dump that I have.
#
# Commented-out ones should not be set in the standard configuration
# procedure, for a variety of reasons:
# 1) They're read-only (status, feedback, etc)
# 2) Not applicable to our use case. (control modes we don't use,
#    features our setup doesn't have.) If I was in doubt about whether
#    it applied, I left it in.
# 3) Default values don't make sense, because they'll be set at
#    runtime. (velocity command, trajectory goals)
#
# (I provide the dumps for some of these, but we don't want to wait
# for them to be written during the configuration step.)
#
# Expected configuration routine is:
# 1) set all parameters in flash, read back value
# 2) dump all read-back values to a timestamped config file
# 3) reset the servo using "r\r\n" (this re-populates ram from flash)


0x00: 300 # Current Loop Proportional Gain (Cp)
0x01: 50  # Current Loop Integral Gain (Ci)
# 0x02 # Current loop programmed value. (N/A, runtime)
# 0x03 # Winding A Current (READ ONLY)
# 0x04 # Winding B Current (READ ONLY)
# 0x05 # Current Offset A  (READ ONLY)
# 0x06 # Current Offset B (READ ONLY)
# 0x07 # X axis of calculated stator current vector (READ ONLY)
# 0x08 # Y axis of calculated stator current vector (READ ONLY)
# 0x09 # Current loop output, X axis of stator space (READ ONLY)
# 0x0a # Current loop output, Y axis of stator space (READ ONLY)
# 0x0b # Actual Current, D axis of rotor space (READ ONLY)
# 0x0c # Actual Current, Q axis of rotor space  (READ ONLY)
# 0x0d # Commanded current, D axis of rotor space (READ ONLY)
# 0x0e # Commanded Current, Q axis of rotor space (READ ONLY)
# 0x0f # Current Error, D axis of rotor space (READ ONLY)

# 0x10 # Current Error, Q axis of rotor space (READ ONLY)
# 0x11 # Current Integral Value, D axis of rotor space (READ ONLY)
# 0x12 # Current Integral Value, Q axis of rotor space (READ ONLY)
# 0x13 # Current Loop Output, D axis of rotor space (READ ONLY)
# 0x14 # Current Loop Output, Q axis of rotor space (READ ONLY)
# 0x15 # Commanded Motor Current (READ ONLY)
# 0x16 ???? -- not documented
# 0x17 # Actual Position (READ ONLY)
# 0x18 # Actual Velocity (READ ONLY)
# 0x19 # Analog reference scaling factor. (N/A)
# 0x1a # Offset Value applied to analog reference input (N/A)
# 0x1b # Analog Encoder Sine Input Voltage (READ ONLY)
# 0x1c # Analog Encoder Cosine Input Voltage (READ ONLY)
# 0x1d # Analog Reference Input Voltage (READ ONLY)
# 0x1e # High Voltage A/D Reading (READ ONLY)

# 0x20 # Dive temperature A/D Reading (READ ONLY)
0x21: 1400 # Peak Current Limit (0.01 A); overwritten by driver
0x22: 280  # Continuous Current Limit (0.01 A); overwritten by driver
0x23: 1322 # Time at Peak Current Limit (ms)
0x24: 0   # Desired State (enum)
# 0x25 # Limited Motor Current Command (READ ONLY)
# 0x26 # analog Reference Input Deadband. (N/A)
0x27: 32000 # Velocity Loop Proportional Gain (Vp)
0x28: 0 # Velocity Loop Integral Gain (Vi)
# 0x29 # Velocity Loop Limited Velocity (READ ONLY)
# 0x2a # Velocity Loop Error (READ ONLY)
# 0x2b # Velocity loop Integral Sum (READ ONLY)
# 0x2c # Commanded Velocity (READ ONLY)
# 0x2d # Commanded Position (READ ONLY)
0x2e: 0  # Velocity Loop Acceleration Feed Forward
# 0x2f # Programmed Velocity Command (0.1 encoder counts /sec) (runtime)

0x30: 100 # Position Loop Proportional Gain (Pp)
0x31: 0  # Velocity Loop Shift Value
# 0x32 # Actual Motor Position (READ ONLY)
0x33: 16384 # Position Loop Velocity Feed Forward (Vff)
0x34: 0     # Position Loop Acceleration Feed Forward (Aff)
# 0x35 # Position Loop Error (READ ONLY)
# TODO: Query 0x36 and 0x37!
0x36: 28 # Veloctiy Loop Acceleration Limit (1000 counts/s^2)
0x37: 28 # Velocity Loop Deceleration Limit (1000 counts/s^2)
# 0x38 # Actual Motor Current (READ ONLY)
0x39: 38 # Velocity Loop Emergency Stop Deceleration Rate (1000 counts/s^2)
0x3a: 28000 # Velocity Loop Velocity Limit (0.1 counts/s)
# 0x3b # Instantaneous Commanded Velocity (READ ONLY)
# 0x3c # Instantaneous Commanded Acceleration (READ ONLY)
# 0x3d # Trajectory Destination Position (READ ONLY)
0x3e: 14000 # Velocity Window (0.1 counts/s)
0x3f: 100   # Velocity Window Time (ms)

0x40: 48           # Motor Type (bit mapped)
0x41: MAXON        # Motor Manufacturer (string)
0x42: EC-60-412825 # Motor Model (string)
0x43: 0            # Motor Units (enum, metric/english)
0x44: 1210000  # Motor Inertia (Mass) (0.000001 Kg/cm 2)
0x45: 7 # Motor Poll Pairs (used only for rotary motors)
0x46: 1 # Motor Brake Type (enum; 1 = None)
0x47: 0 # Motor Temperature Sensor Type (enum; 0 = None)
0x48: 11400 # Motor Torque Constant (0.00001 Nm/A)
0x49: 110 # Motor Resistance (10 mOhm)
0x4a: 86  # Motor Inductance (10 uH)
0x4b: 501000 # Motor Peak Torque (0.00001 Nm)
0x4c: 31900  # Motor Continuous Torque (0.00001 Nm)
0x4d: 28000  # Motor Max Velocity (0.1 encoder counts/s)
0x4e: 0 # Motor Wiring (enum, 0 = standard, 1 = swapped U and V)
0x4f: 0 # Motor Hall Offset

0x50: 1   # Motor Hall Type (0 - none; 1 - digital; 2 -analog)
0x52: 116 # Motor Hall Wiring (bit mapped)
# 0x53: 0 # Motor Brake Activation Time (N/A) (ms)
# 0x54: 0 # Motor Brake Delay Time (N/A) (ms)
# 0x55: 0 # Motor Brake Activation Velocity (0.1 counts/s)
0x56: 1194 # Motor Back EMF Constant
# 0x57 # Microsteps/Motor Rev (microsteps) (N/A)
# 0x58: 65537 # Motor Gear Ratio (N/A - only affects CME display)
# 0x59: # Hall Velocity Mode Shift Value. (N/A - only hall velocity mode)
# 0x5a: 1 # Encoder output configuration (bit mapped)
# 0x5b # Load encoder resolution, linear motors only (N/A)
0x5c: 1 # Load Encoder Direction (0-normal; 1-reverse)
0x5d: 12 # Load Encoder Type
# 0x5e # Load Encoder Velocity (0.1 counts/s) (READ ONLY)
0x5f: 0x20110064 0x00010101 8.54081e-01 0.00000e+00 7.29597e-02 7.29597e-02 0.00000e+00  # Velocity Loop Output Filter

0x60: 6 # Motor Encoder Type (enum; 6 - digital halls)
# 0x6 # Motor Encoder Units (N/A for rotary motors)
0x62: 4000 # Motor Encoder Counts/Rev
# 0x63 # Motor Encoder Resolutio (N/A - linear only)
# 0x64 # Motor Encoder Electrical Distance (N/A - linear only)
# 0x65 # Motor Encoder Direction
# 0x66 ???? -- not documented
0x67: 0 # Analog Encoder Shift Amount
# 0x68 # Captured Index Position (READ ONLY)
# 0x69 # Unfiltered Motor Encoder Velocity  (READ ONLY)
0x6a: 0 # Commanded Current Ramp Limit (mA/s)
0x6b: 0xe10100c8 0x00010101 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 # Velocity Loop Command Filter Coefficients
0x6c: 0 # Position Capture Control Register
# 0x6d # Position Capture Status Register (READ ONLY)
0x6e: 1 # Number of Resolver Cycles/Motor Rev (N/A)
# 0x6f # PWM Mode and Status (N/A)

0x70: 2 0 # Output 0 Configuration
0x71: 2 0 # Output 1 Configuration
0x72: 0 # Output 2 Configuration
0x73: 0 # Output 3 Configuration
0x74: 0 # Output 4 Configuration
0x75: 0 # Output 5 Configuration
0x76: 0 # Output 6 Configuration
0x77: 0 # Output 7 Configuration
0x78: 0 # Input 0 Configuration
0x79: 0 # Input 1 Configuration
0x7a: 0 # Input 2 Configuration
0x7b: 0 # Input 3 Configuration
0x7c: 0 # Input 4 Configuration
0x7d: 0 # Input 5 Configuration
0x7e: 0 # Input 6 Configuration
0x7f: 0 # Input 7 Configuration

# 0x80 # Model Number  (READ ONLY)
# 0x81: 22182222 # Drive Serial Number  (READ ONLY)
# 0x82 # Drive's rated Peak Current (0.01 A) (READ ONLY)
# 0x83 # Drive's rated Continuous Current (0.01 A) (READ ONLY)
# 0x84 # Current Corresponding to Max A/D Reading (0.01 A) (READ ONLY)
# 0x85 # PWM Period (10 ns) (READ ONLY)
# 0x86 # Drive Servo Period (PWM periods)  (READ ONLY)
# 0x87 # Product Family  (READ ONLY)
# 0x88 # Drive's rated Time At Peak Current (ms)  (READ ONLY)
# 0x89 # Drive's rated Maximum Voltage (0.1 V)  (READ ONLY)
# 0x8a # Drive's rated Voltage Corresponding To Max A/D Reading (0.1 V)  (READ ONLY)
# 0x8b # Drive's rated Minimum Voltage (0.1 V)  (READ ONLY)
# 0x8c # Drive's rated Maximum Temperature (C)  (READ ONLY)
# 0x8d # Manufacturing info (date code, etc.)  (READ ONLY)
# 0x8e # Analog Input Scaling Factor  (READ ONLY)
# 0x8f ???? - not documented

# 0x90 # Serial Port Baud Rate (defaults to 9600 at reset) (N/A)
# 0x91 # The maximum number of data words allowed per binary command over the serial interface (READ ONLY)
# 0x92 # Drive Name (string) (N/A)
# 0x93 ???? -- not documented
# 0x94 # Firmware Version Number (READ ONLY)
# 0x95 # Host Configuration State (N/A - CME2 software only)
# 0x96: -12 # Calibration Offset For Analog Reference (N/A)
# 0x97 # Hysteresis Value For Drive Over Temperature Cut-Out (READ ONLY)
# 0x98 # Function Generator Configuration (N/A)
# 0x99 # Function Generator Frequency (N/A)
# 0x9a # Function Generator Amplitude (N/A)
# 0x9b # Function Generator Duty Cycle (N/A)
# 0x9c # Hysteresis For Maximum Bus Voltage Cut-Out (READ ONLY)
# 0x9d # PWM Dead Time At Continuous Current Limit (READ ONLY)
# 0x9e # Drive Minimum PWM Off Time (READ ONLY)
# 0x9f # PWM Dead Time At Zero Current (READ ONLY)

# 0xa0 # Drive Event Status Register (READ ONLY)
# 0xa1 # Latched Event Status Register (N/A - runtime)
# 0xa2 # Hall Input State (READ ONLY)
# 0xa3 ???? - not documented
# 0xa4 # Latching Fault Status Register (N/A - runtime)
0xa5: 0 # Input Pin Configuration Register
# 0xa6 # Input Pin States  (READ ONLY)
0xa7: 31 # Fault Mask
0xa8: 0 # Digital Input Command Configuration
# 0xa9: 1 # Digital Command Input Scaling Factor (N/A - PWM mode)
# 0xaa # Raw Input State (READ ONLY)
# 0xab # Output States And Program Control (N/A - runtime)
# 0xac # Sticky Drive Event Status Register (READ ONLY)
# 0xad # Drive Hardware Type (READ ONLY)
0xae: 0 # Current Loop Offset (0.01 A)
0xaf: 2 # Miscellaneous Drive Options Register (bit mapped)

# 0xb0 # Motor Phase Angle (N/A - runtime, micro-stepping mode)
# 0xb1 # Increment Rate in Micro Stepping Mode (N/A)
0xb2: 7 # Commutation Mode (enum: 7 - trapezoidal commutation with phase angle commutation)
# 0xb3 # Analog Encoder Scaling Factor  (READ ONLY)
# 0xb4 # Encoder Phase Angle (READ ONLY)
# 0xb5 # Homing Adjustment (READ ONLY)
# 0xb6 # PWM Input Frequency (Hz) (N/A)
# 0xb7 # System Time (READ ONLY)
0xb8: 0 # Positive Software Limit (set to less than negative limit to disable)
0xb9: 1 # Negative Software Limit (set to greater than positive limit to disable)
0xba: 2048 # Position Tracking Error Limit (counts)
0xbb: 512 # Position Tracking Warning Limit (counts)
0xbc: 40 # Position Tracking Window Limit (counts)
0xbd: 10 # Time Delay For Position Tracking Error Limit (ms)
0xbe: 0 # Software Limit Deceleration (10 counts / s^2)
0xbf: 100 # Homing Current Delay Time (used with home to hard stop mode only)

# 0xc0 # Network Node ID (READ ONLY)
0xc1: 2048 # Network Node ID Configuration
0xc2: 15 # Homing Method Configuration (bit mapped)
0xc3: 700 # Homing Velocity (fast moves) (0.1 counts / s)
0xc4: 140 # Homing Velocity (slow moves) (0.1 counts / s)
0xc5: 139 # Homing Acceleration/Deceleration (10 counts / s^2)
0xc6: 0 # Home Offset (counts); overwritten by driver
0xc7: 140 # Homing Current Limit (home to hard stop mode only) (0.01 A)
0xc8: 0 # Trajectory Profile Mode (0 - absolute move, trapezoidal profile)
# 0xc9 # Trajectory Status Register  (READ ONLY)
# 0xca # Trajectory Generator Position Command (N/A - runtime)
0xcb: 8000 # Trajectory Maximum Velocity (0.1 counts / s)
0xcc: 819 # Trajectory Maximum Acceleration (10 counts / s^2)
0xcd: 819 # Trajectory Maximum Deceleration
# 0xce Trajectory Maximum Jerk (N/A - s-curve profile only)
0xcf: 1399 # Trajectory Abort Deceleration (10 counts / s^2)

0xd0: 0 # Input 8 Configuration
0xd1: 0 # Input 9 Configuration
0xd2: 0 # Input 10 Configuration
0xd3: 0 # Input 11 Configuration
0xd4: 0 # Input 12 Configuration
0xd5: 0 # Input 13 Configuration
0xd6: 0 # Input 14 Configuration
0xd7: 0 # Input 15 Configuration
# 0xd8 # Regen Resistor Resistance (N/A)
# 0xd9 # Regen Resistor, Continuous Power (N/A)
# 0xda # Regen Resistor, Peak Power (N/A)
# 0xdb # Regen Resistor, Time At Peak (N/A)
# 0xdc # Regen Turn On Voltage Units (N/A)
# 0xdd # Regen Turn Off Voltage (N/A)
# 0xde # Drive's Peak Current Rating For Internal Regen Transistor  (READ ONLY)
# 0xdf # Drive's Continuous Current Rating For Internal Regen Transistor (READ ONLY)

# 0xe0 # Drive's Time At Peak Current For Its Internal Regen Transistor (READ ONLY)
# 0xe1 # Regen Resistor Model Number String (N/A)
# 0xe2 # Regen Resistor Status (READ ONLY)
0xe3: 100 # Position Loop Output Gain Multiplier (100 = 1.0x scaling factor)
0xe4: 560 # Maximum Current to use with algorithmic phase initialization (0.01 A)
0xe5: 400 # Algorithmic Phase Initialization Timeout (ms)
# 0xe6: # Maximum Velocity Adjustment (N/A - stepper loop only)
# 0xe7: # proportional gain for stepper outer loop (N/A)
# 0xe8 # Holding current for microstepping mode (0.01 A) (N/A)
# 0xe9 # Run to hold time for microstepping mode (ms) (N/A)
# 0xea # Detent correction gain for microstepping mode (N/A)
# 0xeb ???? - not documented
# 0xec ???? - not documented
# 0xed # Holding current to fixed voltage output for microstepping (N/A)
# 0xee # Stepper configuration and status (N/A)
# 0xef ???? - not documented

# 0xf0 # Debounce Time For Input 0 (ms) (N/A)
# 0xf1 # Debounce Time For Input 1
# 0xf2 # Debounce Time For Input 2
# 0xf3 # Debounce Time For Input 3
# 0xf4 # Debounce Time For Input 4
# 0xf5 # Debounce Time For Input 5
# 0xf6 # Debounce Time For Input 6
# 0xf7 # Debounce Time For Input 7
# 0xf8 # Debounce Time For Input 8
# 0xf9 # Debounce Time For Input 9
# 0xfa # Debounce Time For Input 10
# 0xfb # Debounce Time For Input 11
# 0xfc # Debounce Time For Input 12
# 0xfd # Debounce Time For Input 13
# 0xfe # Debounce Time For Input 14
# 0xff # Debounce Time For Input 15

# 0x100 # CANopen Limit Status Mask (N/A)
# 0x101 # Network Address Switch Value (READ ONLY)
# 0x102 # Network Status Word (READ ONLY)
# 0x103 # Input Pin Mapping For Node ID Selection (N/A)
0x104: 0 # Algorithmic Phase Initialization Config
# 0x105 # Camming Configuration (N/A)
# 0x106 # Camming delay, forward motion (N/A)
# 0x107 # Camming delay, reverse motion (N/A)
# 0x108 # Writing any value to this parameter will cause any CANopen PDO objects configured with type code 254 to be sent (N/A)
# 0x109 # Camming master velocity (N/A)
# 0x10a # Captured Home Position (READ ONLY)
# 0x10b # Firmware Version Number (READ ONLY)
# 0x10c # CANopen Heartbeat Time (N/A)
# 0x10d # CANopen Node Guarding Time (N/A)
# 0x10e # CANopen Node Guarding Life Time Factor (N/A)
# 0x10f # Registration Offset For Pulse & Direction Mode (N/A)


# 0x110 # Time Stamp of Last High Speed Position Capture (N/A)
# 0x111 # Captured Position for High Speed Position Capture (READ ONLY)
# 0x112 # Position Encoder Position (N/A)
# 0x113 # CANopen emergency inhibit time (N/A)
0x114: 0 # Velocity loop Drain (integral bleed)
# 0x115 # Trajectory buffer access (N/A)
# 0x116 # CANopen quick stop option code  (N/A)
# 0x117 # CANopen shutdown option code  (N/A)
# 0x118 # CANopen disable option code (N/A)
# 0x119 # CANopen halt option code (N/A)
# 0x11a # Drive scaling configuration (READ ONLY)
# 0x11b - 0x11f: ???? not documented


# 0x120 # Returns the number of axis implemented by this drive (READ ONLY)
0x121: 0 # Network options
# 0x122 # Internal maximum regen current (READ ONLY, N/A)
0x123: 1 # Motor encoder wrap position
0x124: 4096 # Load encoder wrap position
# 0x125 # Configures the MACRO drive's encoder capture circuit (N/A)
# 0x126 # FPGA firmware version number (READ ONLY)
0x127: 0 # Gain scheduling configuration
# 0x128 # Gain scheduling key parameter value (N/A - control, not config)
# 0x129 # Drive Hardware Options (N/A)
0x12a: 0 # Motor encoder options
0x12b: 25496332 # Load Encoder Options
# 0x12c # Secondary firmware version for drives equipped with two processors (READ ONLY)
0x12d: 0xe10100c8 0x00010101 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 # Analog Input Filter Coefficients
# 0x12e # Motor encoder status (READ ONLY)
# 0x12f # Load encoder status (READ ONLY)


0x130: 0 # RMS current calculation period
# 0x131 # RMS current over the period set in parameter 0x130  (READ ONLY)
# 0x132 # Running sum of user current limit in 0  (READ ONLY)
# 0x133 # Running sum of amp current limit in 0 (READ ONLY)
# 0x134 # Analog output configuration (N/A)
# 0x135 # Analog output value (N/A)
# 0x136 # Secondary analog reference value (READ ONLY)
# 0x137 # Secondary analog reference offset (N/A)
# 0x138 # Calibration offset for second analog reference input (N/A)
# 0x139 # Status of drive safety circuit (N/A - status)
# 0x13a # Present voltage at analog motor temperature sensor  (READ ONLY)
0x13b: 0 # Limit for analog motor temperature sensor
# 0x13c # Minimum PWM pulse width in microseconds (N/A - pwm position mode)
# 0x13d # Maximum PWM pulse width used when running in PWM position mode (N/A)
# 0x13e, 0x13f: ???? not documented


# 0x140 - 0x14f: ??? not documented

0x150: 0xe00100c8 0x00010101 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 # Second chained biquad filter on output of velocity loop
0x151: 0xe00100c8 0x00010101 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 # Third chained biquad filter on output of velocity loop
0x152: 0xe00100c8 0x00010101 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00  # First chained biquad filter on input of current loop
0x153: 0xe00100c8 0x00010101 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 # Second chained biquad filter on input of current loop
0x154: 0 # Servo loop configuration
0x155: 0 # Position loop integral gain (KI)
0x156: 0 # Derivative gain for position loop (KD)
0x157: 0 # Velocity loop command feed forward
0x158: 0 # Integral drain for position loop
0x159: 1 # Abort option code for CANopen / EtherCAT drives
0x15a: 1 # I/O options
# 0x15b # Motor brake enable delay time (milliseconds) (N/A)
# 0x15c # 32-bit version of parameter 0xA6 (READ ONLY)
# 0x15d # 32-bit version of parameter 0xAA (READ ONLY)
# 0x15e # 32-bit version of parameter 0xA5
# 0x15f: ??? not documented

# 0x160 # Input pin config for general purpose input 16 (N/A)
# 0x161 # Input pin config for general purpose input 17 (N/A)
# 0x162 # Input pin config for general purpose input 18 (N/A)
# 0x163 # Input pin config for general purpose input 19 (N/A)
# 0x164 # Input pin config for general purpose input 20 (N/A)
# 0x165 # Input pin config for general purpose input 21 (N/A)
# 0x166 # Input pin config for general purpose input 22 (N/A)
# 0x167 # Input pin config for general purpose input 23  (N/A)

# 0x170 # Debounce time for general purpose input 16  (N/A)
# 0x171 # Debounce time for general purpose input 17 (N/A)
# 0x172 # Debounce time for general purpose input 18 (N/A)
# 0x173 # Debounce time for general purpose input 19 (N/A)
# 0x174 # Debounce time for general purpose input 20 (N/A)
# 0x175 # Debounce time for general purpose input 21 (N/A)
# 0x176 # Debounce time for general purpose input 22 (N/A)
# 0x177 # Debounce time for general purpose input 23 (N/A)

# 0x180 - 0x182: ???? not documented
# 0x183 # Raw counter value from pulse & direction input hardware (N/A)
# 0x184 # Input shaping filter (N/A)
# 0x185 # Output compare configuration (N/A)
# 0x186 # Output compare status  (N/A)
# 0x187 # Output compare value 1 (N/A)
# 0x188 # Output compare value 2 (N/A)
# 0x189 # Output compare increment (N/A)
# 0x18a # Output compare pulse width (N/A)
# 0x18b # Trajectory options (N/A)
# 0x18c # I/O extension configuration for modules (N/A)
# 0x18d # I/O extension transmit data  (N/A)
# 0x18e # I/O extension receive data  (N/A)
# 0x18f # Encoder sine offset (N/A)



# 0x190 # Encoder cosine offset (N/A)
# 0x191 # Encoder cosine scaling factor (N/A)
# 0x192 # Motor encoder calibration settings (N/A)
# 0x193 # Load encoder calibration settings (N/A)
# 0x194 # PWM input duty cycle (READ ONLY)
0x195: 5594 # Jerk value to use during trajectory aborts
# 0x196 # magnitude squared of the analog encoder signals (READ ONLY)
# 0x197 # Cross coupling KP gain (N/A - dual axis drives)
# 0x198 # Cross coupling KI gain (N/A - dual axis drives)
# 0x199 # Cross coupling KD gain (N/A - dual axis drives)
0x19a: 0 0 0 # Steinhart constants for motor analog motor temperature sensor
# 0x19b # Current at which minimum PWM deadtime is used (READ ONLY)
# 0x19c # High speed capture for passive load encoder (READ ONLY)
# 0x19d # Open motor wiring check current (N/A - requires brake)
0x19e: 0 # Position error timeout (m/s)
# 0x19f: ???? not documented


# 0x1a0 # Output 9 Configuration (N/A)
# 0x1a1 # Output 10 Configuration (N/A)
# 0x1a2 # Output 11 Configuration (N/A)
# 0x1a3 # Output 12 Configuration (N/A)

0x1a8: 0 # Motor encoder down-shift
0x1a9: 0 # Load encoder down-shift
