# sentry_config

This is the core configuration package

## Contents of this Package

* Launch files
* Configuration files
* XACRO to generate Sentry's URDF stuff

## Getting Started

This is a DSL ROS package intended to be run as part of the larger DSL ROS echosystem.
See the [Sentry Wiki](http://sentry-wiki.whoi.edu/ROS_Upgrade)

### Prerequisites

This package depends on pretty much everything required to run Sentry.  That's part of the idea.

### Installing

You should generally be installing this as part of a rosinstall file.  
If handled seperately though, simply add to a catkin workspace and 
build with:

```
catkin_make
```

## Using

`sentry_config` provides both launch files and node configuration files for the Sentry AUV.

- `config`:  contains a hierachy of yaml config files and xml files intended to be included
  in larger launch files.
- `launch`:  contains the main "entry points" of Sentry ROS system
- `machine`: contains name-IP mappings between the various hosts on the sentry network.  No longer used.


### Launch Files:

The core launch file is sentry.launch.  It has a number of options, and can be started in different modes.
Here's an explanation of all the modes.  Note that all of these assume you're running out of a previously
built catkin workspace.

#### Mission Configuration

```
[on mainstack]> source devel/sentry.bash
[on mainstack]> roslaunch sentry_config sentry.launch
```

Simply start without any other configuration options.  Will bring the ROS system up ready to go for a real launch

#### Decktest Configuration

```
[on mainstack]> source devel/sentry.bash
[on mainstack]> roslaunch sentry_config sentry.launch decktest:=true
```

Start the vehicle's normal configuration with any additional nodes or options to run the MC-driven decktest.
This mode also spins up the decktest deadreck node and sets the nav aggregator to use that instead of the
actual DVL.  Note that the DVL must be pinging and producing invalid ranges for the decktest to proceed.

#### Sentry-in-the-loop Simulation

```
[on mainstack]> source devel/sentry.bash
[on mainstack]> roslaunch sentry_config sentry_sim_onsentry.launch
```

```
[on sim machine]> source devel/workstation.bash
[on sim machine]> roslaunch sentry_config sentry_sim_onsentry.launch
```

This mode starts up a Sentry-in-the-loop simulation.  Most software interacts with the actual vehicle hardware, and the 
simulator listens in on those actuator commands to move the vehicle around the simulated environment.  The simulator 
also provides simulated data for a limited subset of sensors.  

The gazebo simulation itself and visualization tools do not run on the mainstack; instead, a seperate sim machine
with a full ros-desktop install is required.

#### No-Hardware Simulation

```
[on dev machine]> source devel/setup.bash
[on dev machine]> roslaunch sentry_config sentry_sim.launch
```

Its also helpful to run just the simulator with no hardware drivers.  This mode is supported via its own launch file.  
It will start all hardware-independent nodes and the gazebo simulator.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Ian Vaughn** - *Initial work* - [WHOI email](mailto:ivaughn@whoi.edu)

## Acknowledgments

* IFREMER for their architectural support
* Louis Whitcomb et. al. for his message definitions to look over


