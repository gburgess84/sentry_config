#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

source $DIR/workstation_setup.bash

ROBOTNAME=sentry
PWRSERVICE=hotel/grd/pwr/mission_powerdown

# Disable
echo "Software disable..."
rosservice call /$ROBOTNAME/abort_cmd 4

echo "Powering off thrusters, servos, and sonars..."
rosservice call /$ROBOTNAME/$PWRSERVICE

echo "Done. This command does not secure burnwires/dcams"

exit
