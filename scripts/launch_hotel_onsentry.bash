#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

# Setup our origin file
python $DIR/link_origin.py $HOME/code/mc/src /tracks/ros_org.yaml
if [ $? != 0 ]
then
  echo "Could not set origin, aborting launch!"
  exit -1  
else
  echo "Origin set successfully!"
fi

source $DIR/sentry_setup.bash

echo "Launching sentry hotel and logging nodes..."
roslaunch sentry_config sentry.launch vehicle_launch:=false

