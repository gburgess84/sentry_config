#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

source $DIR/sentry_setup.bash

ROBOTNAME=sentry
ZERO_PARO=sensors/parodigiquartz/zero_paro

# Zero paro
echo "Zeroing paro..."
rosservice call /$ROBOTNAME/$ZERO_PARO

echo "Done zeroing."

exit
