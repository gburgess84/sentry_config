#!/bin/bash

# SS - created
# First cl argument: name of the session to connect (i.e. dsl)
# Second cl argument: name of the window to connect to within the session

tmux select-window -t $1:$2
tmux -2 attach-session -t $1
