#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

source $DIR/workstation_setup.bash

ROBOTNAME=sentry
PORT_SNUFF=hotel/grd/xr2/cmd 3
DESCENT_SNUFF=hotel/grd/xr1/cmd 3
STARBOARD_SNUFF=hotel/grd/xr1/cmd 3

# Snuff Burnwires
echo "Snuff port..."
rosservice call /$ROBOTNAME/$PORT_SNUFF

echo "Snuff descent..."
rosservice call /$ROBOTNAME/$DESCENT_SNUFF

echo "Snuff starboard..."
rosservice call /$ROBOTNAME/$STARBOARD_SNUFF

echo "Done snuffing."

exit
