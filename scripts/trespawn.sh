#!/bin/bash

# SS - created
# First cl argument: name of the target session (i.e. dsl)
# Second cl argument: name or number of the window to kill and respawn within the session

tmux respawn-window -k -t $1:$2
